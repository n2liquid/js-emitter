module.exports = function LineWriter ()
{
	this.indent = 0;
	this.lines = [];

	LineWriter.prototype.write = function (strings, indent_if_single_line)
	{
		if (strings === undefined)
		{
			return;
		}

		if (! Array.isArray (strings))
		{
			strings = [strings];
		}

		if (indent_if_single_line && strings.length === 1)
		{
			++this.indent;
		}

		var indentation = Array (this.indent + 1).join ('    ');

		strings.forEach
		(
			function (string)
			{
				this.lines.push (indentation + string);
			},

			this
		);

		if (indent_if_single_line && strings.length === 1)
		{
			--this.indent;
		}
	};
};