js-emitter (discontinued)
==========

Pretty JavaScript code generated from syntax trees.

Discontinuation
---

I started this project before I knew of [Escodegen,](https://github.com/Constellation/escodegen) and I'm keeping it just because I think it can be useful to me in the future. If you're looking for a real JavaScript code generation solution, you should definitely check Escodegen out.
