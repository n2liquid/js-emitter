var vows = require ('vows');
var assert = require ('assert');
var emit = require ('../lib/main').emit;

vows.describe ('Expressions').addBatch
(
	{
		'Name':
		{
			'Name': function ()
			{
				var node = { type: 'name', identifier: 'lolicon' };
				var output = emit (node);

				assert.equal (output, 'lolicon');
			}
		},

		'Call':
		{
			'With name expression as callable and no arguments': function ()
			{
				var node =
				{
					type: 'call',

					callable: { type: 'name', identifier: 'desu' }
				};

				var output = emit (node);

				assert.equal (output, 'desu ()');
			},

			'With name expression as callable and 1 argument': function ()
			{
				var node =
				{
					type: 'call',

					callable: { type: 'name', identifier: 'nano' },

					args: { type: 'literal (string)', value: 'desu' }
				};

				var output = emit (node);

				assert.equal (output, 'nano ("desu")');
			},

			'With name expression as callable and multiple arguments': function ()
			{
				var node =
				{
					type: 'call',

					callable: { type: 'name', identifier: 'nano' },

					args:
					[
						{ type: 'literal (string)', value: 'desu' },

						{ type: 'literal (number)', value: 1 },

						{ type: 'literal (number)', value: 2 },

						{ type: 'literal (number)', value: 3 }
					]
				};

				var output = emit (node);

				assert.equal (output, 'nano ("desu", 1, 2, 3)');
			}
		},

		'Unary':
		{
			'Logical Not': function ()
			{
				var node =
				{
					type: 'unary', operator: '!',

					expression: { type: 'literal (boolean)', value: false }
				};

				var output = emit (node);

				assert.equal (output, '!false');
			},

			'Bitwise Not': function ()
			{
				var node =
				{
					type: 'unary', operator: '~',

					expression: { type: 'literal (number)', value: 13 }
				};

				var output = emit (node);

				assert.equal (output, '~13');
			},

			'Plus': function ()
			{
				var node =
				{
					type: 'unary', operator: '+',

					expression: { type: 'literal (number)', value: 1 }
				};

				var output = emit (node);

				assert.equal (output, '+1');
			},

			'Minus': function ()
			{
				var node =
				{
					type: 'unary', operator: '-',

					expression: { type: 'literal (number)', value: 1 }
				};

				var output = emit (node);

				assert.equal (output, '-1');
			},

			'Invalid operator (should throw)': function ()
			{
				var node =
				{
					type: 'unary', operator: '?',

					expression: { type: 'literal (number)', value: 1 }
				};

				assert.throws
				(
					function ()
					{
						emit (node);
					}
				);
			}
		},

		'Binary':
		{
			'Addition': function ()
			{
				var node =
				{
					type: 'binary', operator: '+',

					left: { type: 'literal (number)', value: 1 },

					right: { type: 'literal (number)', value: 2 }
				};

				var output = emit (node);

				assert.equal (output, '1 + 2');
			},

			'Subtraction': function ()
			{
				var node =
				{
					type: 'binary', operator: '-',

					left: { type: 'literal (number)', value: 2 },

					right: { type: 'literal (number)', value: 1 }
				};

				var output = emit (node);

				assert.equal (output, '2 - 1');
			},

			'Multiplication': function ()
			{
				var node =
				{
					type: 'binary', operator: '*',

					left: { type: 'literal (number)', value: 2 },

					right: { type: 'literal (number)', value: 3 }
				};

				var output = emit (node);

				assert.equal (output, '2 * 3');
			},

			'Division': function ()
			{
				var node =
				{
					type: 'binary', operator: '/',

					left: { type: 'literal (number)', value: 4 },

					right: { type: 'literal (number)', value: 2 }
				};

				var output = emit (node);

				assert.equal (output, '4 / 2');
			},

			'Modulo': function ()
			{
				var node =
				{
					type: 'binary', operator: '%',

					left: { type: 'literal (number)', value: 2 },

					right: { type: 'literal (number)', value: 3 }
				};

				var output = emit (node);

				assert.equal (output, '2 % 3');
			},

			'Relational (less than)': function ()
			{
				var node =
				{
					type: 'binary', operator: '<',

					left: { type: 'literal (number)', value: 1 },

					right: { type: 'literal (number)', value: 10 }
				};

				var output = emit (node);

				assert.equal (output, '1 < 10');
			},

			'Relational (less than or equal to)': function ()
			{
				var node =
				{
					type: 'binary', operator: '<=',

					left: { type: 'literal (number)', value: 1 },

					right: { type: 'literal (number)', value: 10 }
				};

				var output = emit (node);

				assert.equal (output, '1 <= 10');
			},

			'Relational (greater than)': function ()
			{
				var node =
				{
					type: 'binary', operator: '>',

					left: { type: 'literal (number)', value: 10 },

					right: { type: 'literal (number)', value: 1 }
				};

				var output = emit (node);

				assert.equal (output, '10 > 1');
			},

			'Relational (greater than or equal to)': function ()
			{
				var node =
				{
					type: 'binary', operator: '>=',

					left: { type: 'literal (number)', value: 10 },

					right: { type: 'literal (number)', value: 1 }
				};

				var output = emit (node);

				assert.equal (output, '10 >= 1');
			},

			'Equality (weak)': function ()
			{
				var node =
				{
					type: 'binary', operator: '==',

					left: { type: 'literal (number)', value: 1 },

					right: { type: 'literal (boolean)', value: true }
				};

				var output = emit (node);

				assert.equal (output, '1 == true');
			},

			'Equality (strong)': function ()
			{
				var node =
				{
					type: 'binary', operator: '===',

					left: { type: 'literal (number)', value: true },

					right: { type: 'literal (boolean)', value: true }
				};

				var output = emit (node);

				assert.equal (output, 'true === true');
			},

			'Inequality (weak)': function ()
			{
				var node =
				{
					type: 'binary', operator: '!=',

					left: { type: 'literal (number)', value: 0 },

					right: { type: 'literal (boolean)', value: true }
				};

				var output = emit (node);

				assert.equal (output, '0 != true');
			},

			'Inequality (strong)': function ()
			{
				var node =
				{
					type: 'binary', operator: '!==',

					left: { type: 'literal (null)' },

					right: { type: 'literal (undefined)' }
				};

				var output = emit (node);

				assert.equal (output, 'null !== undefined');
			},

			'Invalid operator (should throw)': function ()
			{
				var node =
				{
					type: 'binary', operator: '?',

					left: { type: 'literal (number)', value: 0 },

					right: { type: 'literal (number)', value: 0 }
				};

				assert.throws
				(
					function ()
					{
						emit (node);
					}
				);
			}
		}
	}
).export(module);