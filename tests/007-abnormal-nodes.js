var vows = require ('vows');
var assert = require ('assert');
var emit = require ('../lib/main').emit;

vows.describe ('Abnormal Nodes').addBatch
(
	{
		'when emitting a node of type "undefined"':
		{
			'it should return an empty string': function ()
			{
				var node = { type: 'undefined' };
				var output = emit (node);

				assert.equal (output, '');
			}
		},

		'when emitting an empty object as node':
		{
			'it should return an empty string': function ()
			{
				var node = {};
				var output = emit (node);

				assert.equal (output, '');
			}
		},

		'when emitting a non-empty object with no type as node':
		{
			'it should throw': function ()
			{
				var node = { nonEmpty: true };

				assert.throws
				(
					function ()
					{
						emit (node);
					}
				);
			}
		},

		'when emitting an undefined variable as node':
		{
			'it should return an empty string': function ()
			{
				var node = undefined;
				var output = emit (node);

				assert.equal (output, '');
			}
		},

		'when emitting a node of invalid type':
		{
			'it should throw': function ()
			{
				var node = { type: '*invalid type*' };

				assert.throws
				(
					function ()
					{
						emit (node);
					}
				);
			}
		}
	}
).export(module);