var vows = require ('vows');
var assert = require ('assert');
var emit = require ('../lib/main').emit;

vows.describe ('Empty AST').addBatch
(
	{
		'when emitting an empty AST':
		{
			'it should return an empty string': function ()
			{
				var empty_ast = {};
				var output = emit(empty_ast);

				assert.equal (output, '');
			}
		}
	}
).export(module);