var vows = require ('vows');
var assert = require ('assert');
var emit = require ('../lib/main').emit;

vows.describe ('Literals').addBatch
(
	{
		'Booleans':
		{
			'True': function ()
			{
				var node = { type: 'literal (boolean)', value: true };
				var output = emit (node);

				assert.equal (output, 'true');
			},

			'False': function ()
			{
				var node = { type: 'literal (boolean)', value: false };
				var output = emit (node);

				assert.equal (output, 'false');
			}
		},

		'Numbers':
		{
			'Integer equality check': function ()
			{
				var node = { type: 'literal (number)', value: 0 };
				var output = emit (node);

				assert.equal (output, '0');
			},

			'Short decimal equality check': function ()
			{
				var node = { type: 'literal (number)', value: 0.5 };
				var output = emit (node);

				assert.equal (output, '0.5');
			},

			'Decimal equality check with exponent': function ()
			{
				var node = { type: 'literal (number)', value: 1e+24 };
				var output = emit (node);

				assert.equal (output, '1e+24');
			},

			'Round decimals are integers': function ()
			{
				var node = { type: 'literal (number)', value: 42.0 };
				var output = emit (node);

				assert.equal (output, '42');
			}
		},

		'Strings':
		{
			'Simple string': function ()
			{
				var node = { type: 'literal (string)', value: 'a simple string' };
				var output = emit (node);

				assert.equal (output, '"a simple string"');
			},

			'Escaped string': function ()
			{
				var node = { type: 'literal (string)', value: '"\t\r\n\b' };
				var output = emit (node);

				assert.equal (output, '"\\"\\t\\r\\n\\b"');
			}
		},

		'Others':
		{
			'Undefined': function ()
			{
				var node = { type: 'literal (undefined)' };
				var output = emit (node);

				assert.equal (output, 'undefined');
			},

			'Null': function ()
			{
				var node = { type: 'literal (null)' };
				var output = emit (node);

				assert.equal (output, 'null');
			}
		}
	}
).export(module);