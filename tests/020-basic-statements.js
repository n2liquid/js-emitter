var vows = require ('vows');
var assert = require ('assert');
var emit = require ('../lib/main').emit;

vows.describe ('Basic Statements').addBatch
(
	{
		'Very Simple Statements':
		{
			'Expression Statement': function ()
			{
				var node =
				{
					type: 'expression-statement',

					expression:
					{
						type: 'call',

						callable: { type: 'name', identifier: 'desu' }
					}
				};

				var output = emit (node);

				assert.equal (output, 'desu ();');
			},

			'Return Statement': function ()
			{
				var node =
				{
					type: 'return',

					expression: { type: 'literal (boolean)', value: true }
				};

				var output = emit (node);

				assert.equal (output, 'return true;');
			}
		},

		'Blocks':
		{
			'Empty block': function ()
			{
				var node = { type: 'block' };
				var output = emit (node);

				assert.deepEqual (output, [ '{', '}' ]);
			},

			'Block with statements': function ()
			{
				var node =
				{
					type: 'block',

					statements:
					[
						{
							type: 'expression-statement',

							expression:
							{
								type: 'call',

								callable: { type: 'name', identifier: 'desu' }
							}
						},

						{
							type: 'return',

							expression: { type: 'literal (boolean)', value: true }
						}
					]
				};

				var output = emit (node);

				assert.deepEqual
				(
					output,

					[
						'{',
						'    desu ();',
						'    return true;',
						'}'
					]
				);
			}
		}
	}
).export(module);