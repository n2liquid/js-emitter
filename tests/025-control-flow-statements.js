var vows = require ('vows');
var assert = require ('assert');
var emit = require ('../lib/main').emit;

vows.describe ('Control Flow Statements').addBatch
(
	{
		'Very Simple Statements':
		{
			'If': function ()
			{
				var node =
				{
					type: 'if',

					conditional:
					{
						type: 'binary',

						operator: '===',

						left: { type: 'name', identifier: 'state' },

						right: { type: 'literal (string)', value: 'running' }
					},

					body:
					{
						type: 'block',

						statements:
						[
							{
								type: 'call',

								callable: { type: 'name', identifier: 'alert' },

								args: { type: 'literal (string)', value: 'The program is running.' }
							}
						]
					}
				};

				var output = emit (node);

				assert
				(
					output,

					[
						'if (state === "running")',
						'{',
						'	alert ("The program is running.");',
						'}'
					]
				);
			},

			'If-Else': function ()
			{
				var node =
				{
					type: 'if',

					conditional:
					{
						type: 'binary',

						operator: '===',

						left: { type: 'name', identifier: 'state' },

						right: { type: 'literal (string)', value: 'running' }
					},

					body:
					{
						type: 'block',

						statements:
						[
							{
								type: 'call',

								callable: { type: 'name', identifier: 'alert' },

								args: { type: 'literal (string)', value: 'The program is running.' }
							}
						]
					},

					else_clause:
					{
						type: 'block',

						statements:
						[
							{
								type: 'call',

								callable: { type: 'name', identifier: 'alert' },

								args:
								{
									type: 'literal (string)',
									value: 'The program is not running.'
								}
							}
						]
					}
				};

				var output = emit (node);

				assert
				(
					output,

					[
						'if (state === "running")',
						'{',
						'	alert ("The program is running.");',
						'}',
						'else',
						'{',
						'	alert ("The program is not running.");',
						'}'
					]
				);
			}
		}
	}
).export(module);