'use strict';

var LineWriter = require ('../src/LineWriter');
var array_coerce = require ('../src/ArrayUtil/Coerce');
var array_uncoerce = require ('../src/ArrayUtil/Uncoerce');

function emit (source_tree)
{
	source_tree = array_coerce (source_tree);

	var results = source_tree.map
	(
		function (node)
		{
			if (node === undefined)
			{
				node = {};
			}

			if (node.type === undefined)
			{
				node.type = 'undefined';
			}

			if (node_prototypes[node.type] === undefined)
			{
				throw 'AST node has an invalid type.';
			}

			node.__proto__ = node_prototypes[node.type];
			return node.emit ();
		}
	);

	return array_uncoerce (results);
}

function emit_if_defined (source_tree)
{
	if (source_tree === undefined)
	{
		return undefined;
	}

	return emit (source_tree);
}

var node_prototypes =
{
	'undefined':
	{
		emit: function ()
		{
			if (Object.keys(this).length > 1)
			{
				throw 'AST node of undefined type is not empty.';
			}

			return '';
		}
	},

	'literal (undefined)':
	{
		get precedence() { return 0; },

		emit: function ()
		{
			return 'undefined';
		}
	},

	'literal (null)':
	{
		get precedence() { return 0; },

		emit: function ()
		{
			return 'null';
		}
	},

	'literal (boolean)':
	{
		get precedence() { return 0; },

		emit: function ()
		{
			return String (this.value);
		}
	},

	'literal (number)':
	{
		get precedence() { return 0; },

		emit: function ()
		{
			return String (this.value);
		}
	},

	'literal (string)':
	{
		get precedence() { return 0; },

		emit: function ()
		{
			return '"' + String (this.value) + '"';
		}
	},

	'name':
	{
		get precedence() { return 0; },

		emit: function ()
		{
			return String (this.identifier);
		}
	},

	'call':
	{
		get precedence() { return 2; },

		emit: function ()
		{
			var callable = emit (this.callable);
			var args = array_coerce (emit (this.args));

			return callable + " (" + args.join (", ") + ")";
		}
	},

	'unary':
	{
		get precedence() { return 4; },

		emit: function ()
		{
			switch (this.operator)
			{
				case '!':
				case '~':
				case '+':
				case '-':
					break;

				default:
					throw 'Unary expression has invalid operator';
			}

			return this.operator + emit (this.expression);
		}
	},

	'binary':
	{
		get precedence()
		{
			switch (this.operator)
			{
				case '==':
				case '===':
				case '!=':
				case '!==':
					return 9;

				case '<':
				case '<=':
				case '>':
				case '>=':
					return 8;

				case '+':
				case '-':
					return 6;

				case '*':
				case '/':
				case '%':
					return 5;
			}
		},

		emit: function ()
		{
			switch (this.operator)
			{
				case '==':
				case '===':
				case '!=':
				case '!==':
				case '<':
				case '<=':
				case '>':
				case '>=':
				case '+':
				case '-':
				case '*':
				case '/':
				case '%':
					break;

				default:
					throw 'Binary expression has invalid operator';
			}

			return emit (this.left) + " " + this.operator + " " + emit (this.right);
		}
	},

	'block':
	{
		emit: function ()
		{
			var lw = new LineWriter;

			lw.write ('{');
			++lw.indent;
				if (this.statements !== undefined)
				{
					array_coerce (this.statements).forEach
					(
						function (statement)
						{
							statement = emit (statement);
							array_coerce (statement).forEach (lw.write, lw);
						}
					);
				}
			--lw.indent;
			lw.write ('}');

			return array_uncoerce (lw.lines);
		}
	},

	'expression-statement':
	{
		emit: function ()
		{
			return emit (this.expression) + ';';
		}
	},

	'if':
	{
		emit: function ()
		{
			var conditional = emit (this.conditional);
			var body = emit (this.body);
			var else_clause = emit_if_defined (this.else_clause);

			var lw = new LineWriter;

			lw.write ('if (' + conditional + ')');
			lw.write (body, true);
			lw.write (else_clause, true);

			return lw.lines;
		}
	},

	'else':
	{
		emit: function ()
		{
			var body = emit (this.body);

			var lw = new LineWriter;

			lw.write ('else');
			lw.write (body, true);

			return lw.lines;
		}
	},

	'return':
	{
		emit: function ()
		{
			var expression = emit_if_defined (this.expression);

			var lw = new LineWriter;

			if (expression !== undefined)
			{
				lw.write ('return ' + expression + ';');
			}
			else
			{
				lw.write ('return;');
			}

			return array_uncoerce (lw.lines);
		}
	}
};

// Exports
module.exports.emit = emit;